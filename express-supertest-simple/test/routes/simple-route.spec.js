const assert = require('assert');
const request = require('supertest');

// Aplikasi Express yang akan dites
const app = require('../../src/app')


describe('Simple', function () {

    describe('GET /', function () {

        it('responds with json', async function () {
            const response = await request(app)
                .get('/')       // memanggil path '/'
                .expect(200);   // cek status = 200

            assert(response.body.name, 'Ferry L. H.'); // cek di response, name = 'Ferry L. H.'
        });

    });

});