const express = require('express');

// Ada dua cara Routing di Express
// 1. Routing dengan menggunakan app.route() (app adalah instance dari express)
// 2. Routing dengan dengan menggunakan express.Router()

// Instance dari express.Router()
const router = express.Router();

// Mendaftarkan path '/' dengan handler
router.get('/', getData);

// Handler yang akan dipangggil saat memanggil path '/'
function getData(req, res) {
    let data = {
        name: 'Ferry L. H.'
    }

    res.status(200).json(data);
}

module.exports = router;