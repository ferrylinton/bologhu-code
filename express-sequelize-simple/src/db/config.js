module.exports = {
  development: {
    dialect: 'sqlite',
    storage: 'db.sqlite',
    logging: console.log,
    define: {
      timestamps: true,
      underscored: true
    },
    dialectOptions: {
      bigNumberStrings: true
    },
  },
  test: {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false,
    define: {
      timestamps: true,
      underscored: true
    },
    dialectOptions: {
      bigNumberStrings: true
    }
  }
};