const { Op } = require('sequelize');
const { Authority } = require('../models/index');

let AUTHORITIES = [
  { name: 'AUTHORITY_VIEW' },
  { name: 'AUTHORITY_MODIFY' },
  { name: 'ROLE_VIEW' },
  { name: 'ROLE_MODIFY' },
  { name: 'USER_VIEW' },
  { name: 'USER_MODIFY' },
];


async function initDatas() {
  try {
    await Authority.bulkCreate(AUTHORITIES, { validate: true });
    console.log('Authorities created');
  } catch (err) {
    console.log('failed to create Authorities');
    console.log(err);
  }
}

async function findAll() {
  return await Authority.findAll({ order: [['name', 'ASC']] });
}

module.exports = {
  initDatas,
  findAll
};
