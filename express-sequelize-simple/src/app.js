/** 
 * Modul app
 * 
 * @module app
 * @requires express
 * @requires simpleRouter
 */

/**
 * Modul express
 * 
 * @const
 * @type {express}
 */
const express = require('express');

/**
 * Modul authorityRouter
 * 
 * @const
 * @type {express.Router}
 */
const authorityRouter = require('./routers/authority-router');

/**
 * Membuat aplikasi Express
 * 
 * @const
 * @type {express}
 */
const app = express();

/**
 * Mendaftarkan simpleRouter ke dalam aplikasi Express
 * 
 * @function
 * @name authorityRouter
 * @inner
 * @param {string} path - Url untuk mengakses aplikasi Express
 * @param {express.Router} router - Modul Router aplikasi Express
 */
app.use('/', authorityRouter);

module.exports = app;