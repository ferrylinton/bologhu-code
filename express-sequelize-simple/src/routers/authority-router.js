/** 
 * Modul simpleRouter
 * 
 * @module simpleRouter
 * @requires express
 */

/**
 * Modul express
 * 
 * @const
 * @type {express}
 */
const express = require('express');

const authorityService = require('../services/authority-service');

/**
 * Router untuk mendaftarkan route yang sederhana
 * 
 * @const
 * @type {express.Router}
 */
const router = express.Router();

/**
 * Route untuk menampilkan data sederhana
 * 
 * @function
 * @name GET_findAll
 * @inner
 * @param {string} path - Url untuk mengakses aplikasi Express
 * @param {callback} middleware - Fungsi middleware untuk Router
 */
router.get('/', findAll);

/**
 * Menampilkan data JSON yang sederhana di dalam respon HTTP
 * 
 * @function
 * @param {express.Request} req - Merepresentasikan permintaan HTTP
 * @param {express.Response} res - Merepresentasikan respon dari HTTP
 * @param {express.NextFunction} next - Fungsi middleware untuk aplikasi Express
 */
async function findAll(req, res, next) {
    let authorities = await authorityService.findAll();
    res.status(200).json(authorities);
}

module.exports = router;