'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Authority extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

  Authority.init({

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },

    name: {
      type: DataTypes.STRING(50),
      unique: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'Authority',
    tableName: 't_Authority'
  });

  return Authority;
};