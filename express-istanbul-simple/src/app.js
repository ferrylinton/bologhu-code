const express = require('express');
const simpleRouter = require('./routers/simple-router');

// Instance dari express()
const app = express();

// Mendaftarkan simpleRouter ke instance dari express()
app.use('/', simpleRouter);

module.exports = app;