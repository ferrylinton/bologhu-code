const app = require('./src/app');

// Port yang akan digunakan saat aplikasi jalan
const port = parseInt(process.env.PORT || '3000', 10);

// Menjalankan aplikasi Express
app.listen(port, callback);

// Fungsi yang akan dipanggil saat aplikasi telah jalan
function callback() {
    console.log('####################################################################');
    console.log('NODE_ENV   : ' + process.env.NODE_ENV);
    console.log('address    : ' + JSON.stringify(this.address()));
    console.log('####################################################################');
}