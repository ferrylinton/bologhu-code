# Menjalankan Hello World Dengan ExpressJS + Handlebars

Membuat projek NodeJS + Handlebars yang sederhana. Projek akan mengunakan konfigurasi Handlebars yang sederhana. Konfigurasi layout yang sederhana, dan menampilkan variabel.

## Prasyarat

1. [NodeJS](https://nodejs.dev/)
2. [ExpressJS](https://expressjs.com/)
3. [Nodemon](https://nodemon.io/)
4. [Handlebars](https://handlebarsjs.com/)


## Langkah-Langkah Yang Digunakan

### 1. Buat projek NodeJS

### 2. Tambahkan ExpressJS ke dalam projek

```
npm install express --save
```

### 3. Tambahkan Handlebars ke dalam projek

```
npm install handlebars --save
```

### 4. Ubah berkas **package.json**

```
{
  "name": "expressjs-handlebars-hello-world",
  "version": "1.0.0",
  "description": "Hello World with ExpressJS + Handlebars",
  "main": "index.js",
  "scripts": {
    "start": "node ./src/app.js",
    "dev": "nodemon ./src/app.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [
    "NodeJS",
    "ExpresJS",
    "Hanlebars"
  ],
  "author": "Ferry L. H.",
  "license": "ISC",
  "dependencies": {
    "express": "^4.17.1",
    "express-handlebars": "^5.2.0"
  }
}

```

### 5. Struktur projek yang akan digunakan

```
.
├── package.json
├── package-lock.json
├── README.md
└── src
    ├── app.js
    ├── config
    │   └── handlebars-config.js
    └── views
        ├── about.hbs
        ├── home.hbs
        ├── layouts
        │   └── main.hbs
        └── partials
            └── footer.hbs
```

### 6. Konfigurasi handlebars di berkas **handlebars-config.js**

```
const path = require('path');
const exphbs = require('express-handlebars');


module.exports = function (app) {
    const hbs = exphbs.create({
        layoutsDir: path.join(process.cwd(), 'src', 'views', 'layouts'),
        partialsDir: path.join(process.cwd(), 'src', 'views', 'partials'),
        defaultLayout: 'main',
        extname: '.hbs'
    });

    app.engine('.hbs', hbs.engine);
    app.set('views', path.join(process.cwd(), 'src', 'views'));
    app.set('view engine', 'hbs');
};
```

### 7. Kode html akan ditulis di dalam berkas dengan extensi **.hbs**

Berkas halaman utama **main.hbs**, ini menjadi kerangka utama. Artinya setiap halaman yang akan dipanggil (contoh: **home** atau **about**), akan dimasukkan ke dalam halaman utama.

```
<!DOCTYPE html>
<html lang="id">

<head>
  <title>bologhu</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body style="margin: 0; padding:0; background-color: #cccccc;">
<div style="margin: 0; padding:0;">
  <h2 style="padding: 15px; margin: 0;">Main Layout</h2>
  <div style="padding: 15px;">
    <a style="display: inline-block; color: #000000; font-weight: bold;" href="/">Home</a>
    <a style="display: inline-block; color: #000000; font-weight: bold;" href="/about">About</a>
  </div>
  <div style="margin: 15px; padding: 15px; background-color: #ffffff;">
    {{{body}}} ==> bagian ini akan diganti dengan isi halaman yang dipanggil
  </div>
</div>
</body>

</html>
```

Berkas **footer.hbs**, berisikan potongan html, yang bisa ditambah ke dalam halaman yang dipanggil. Tidak semua halaman akan ditambahkan dengan berkas **footer.hbs**

```
<div style="border: solid 1px #b8b8b8; background-color: #cccccc; padding: 15px;">
  Footer / Partials
</div>
```

Berkas **home.hbs**

```
<h2>Home</h2>
<p>{{date}}</p>
{{> footer}}
```

Berkas **about.hbs**

```
<h2>About</h2>
<p>{{msg}}</p>
```

### 8. Buat berkas **app.js**, tulis kode berikut:

```
const express = require('express')
const handlebarsConfig = require('./config/handlebars-config');

const port = 3000;
const app = express();
handlebarsConfig(app);


app.get('/', (req, res, next) => {
  let data = {
    date : new Date()
  }
  res.render('home', data);
})

app.get('/about', (req, res, next) => {
  let data = {
    msg : 'Horas !!'
  }
  res.render('about', data);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
```

### 9. Jalankan projek dengan perintah

```
npm run dev
```

### 10. Buka browser dan dengan alamat **http://localhost:3000/**