const assert = require('assert');
const request = require('supertest');

// Aplikasi Express yang akan dites
const app = require('../../src/app')


describe('Simple', function () {

    describe('GET /', function () {

        it('responds with Home page html', async function () {
            const response = await request(app)
                .get('/')                       // memanggil path '/'
                .expect('Content-type',/html/)  // cek tipe response
                .expect(200);                   // cek status = 200

            assert(response.text.includes('Home')); // cek di text 'Home' di dalam html
        });

    });

    describe('GET /about', function () {

        it('responds with About page html', async function () {
            const response = await request(app)
                .get('/about')                  // memanggil path '/about'
                .expect('Content-type',/html/)  // cek tipe response
                .expect(200);                   // cek status = 200

            assert(response.text.includes('Horas')); // cek di text 'Horas' di dalam html
        });

    });

});