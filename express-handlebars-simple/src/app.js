const express = require('express');
const handlebarsConfig = require('./config/handlebars-config');
const simpleRouter = require('./routers/simple-router');

// Instance dari express()
const app = express();

// Konfigurasi handlebars dengan express
handlebarsConfig(app);


// Mendaftarkan simpleRouter ke instance dari express()
app.use('/', simpleRouter);

module.exports = app;
