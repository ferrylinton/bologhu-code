# Konfigurasi Bootstrap dengan ExpressJS + Handlebars

Membuat projek ExpressJS dan Handlebars yang sederhana, dan konfigurasi tampilan website dengan menggunakan Bootstrap.

## Prasyarat

1. [NodeJS](https://nodejs.dev/)
2. [ExpressJS](https://expressjs.com/)
3. [Nodemon](https://nodemon.io/)
4. [Handlebars](https://handlebarsjs.com/)
5. [Bootstrap 4.6](https://getbootstrap.com/docs/4.6/getting-started/introduction/)

## Langkah-Langkah Yang Digunakan

### 1. Buat Projek ExpressJS + Handlebar



