const express = require('express');

// Ada dua cara Routing di Express
// 1. Routing dengan menggunakan app.route() (app adalah instance dari express)
// 2. Routing dengan dengan menggunakan express.Router()

// Instance dari express.Router()
const router = express.Router();

// Mendaftarkan path '/' dengan handler
router.get('/', (req, res, next) => {
    let data = {
        date: new Date()
    }
    res.render('home', data);
})

router.get('/about', (req, res, next) => {
    let data = {
        msg: 'Horas !!'
    }
    res.render('about', data);
})

module.exports = router;