const path = require('path');
const exphbs = require('express-handlebars');


module.exports = function (app) {
    const hbs = exphbs.create({
        layoutsDir: path.join(process.cwd(), 'src', 'views', 'layouts'),
        partialsDir: path.join(process.cwd(), 'src', 'views', 'partials'),
        defaultLayout: 'main',
        extname: '.hbs'
    });

    app.engine('.hbs', hbs.engine);
    app.set('views', path.join(process.cwd(), 'src', 'views'));
    app.set('view engine', 'hbs');
};
