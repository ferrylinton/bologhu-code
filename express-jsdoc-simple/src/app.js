/** 
 * Modul app
 * 
 * @module app
 * @requires express
 * @requires simpleRouter
 */

/**
 * Modul express
 * 
 * @const
 * @type {express}
 */
const express = require('express');

/**
 * Modul simpleRouter
 * 
 * @const
 * @type {express.Router}
 */
const simpleRouter = require('./routers/simple-router');

/**
 * Membuat aplikasi Express
 * 
 * @const
 * @type {express}
 */
const app = express();

/**
 * Mendaftarkan simpleRouter ke dalam aplikasi Express
 * 
 * @function
 * @name simpleRouter
 * @inner
 * @param {string} path - Url untuk mengakses aplikasi Express
 * @param {express.Router} router - Modul Router aplikasi Express
 */
app.use('/', simpleRouter);

module.exports = app;