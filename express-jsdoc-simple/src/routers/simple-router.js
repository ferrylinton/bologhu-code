/** 
 * Modul simpleRouter
 * 
 * @module simpleRouter
 * @requires express
 */

/**
 * Modul express
 * 
 * @const
 * @type {express}
 */
const express = require('express');

/**
 * Router untuk mendaftarkan route yang sederhana
 * 
 * @const
 * @type {express.Router}
 */
const router = express.Router();

/**
 * Route untuk menampilkan data sederhana
 * 
 * @function
 * @name GET_getData
 * @inner
 * @param {string} path - Url untuk mengakses aplikasi Express
 * @param {callback} middleware - Fungsi middleware untuk Router
 */
router.get('/', getData);

/**
 * Menampilkan data JSON yang sederhana di dalam respon HTTP
 * 
 * @function
 * @param {express.Request} req - Merepresentasikan permintaan HTTP
 * @param {express.Response} res - Merepresentasikan respon dari HTTP
 * @param {express.NextFunction} next - Fungsi middleware untuk aplikasi Express
 */
function getData(req, res, next) {
    let data = {
        name: 'Ferry L. H.'
    }

    res.status(200).json(data);
}

module.exports = router;