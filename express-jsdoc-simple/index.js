/** 
 * Modul utama
 * 
 * @module index
 * @requires app
 */

/**
 * Modul app
 * 
 * @const
 * @type {express}
 */
const app = require('./src/app');

/**
 * Nomor port saat aplikasi dijalankan
 * 
 * @const
 * @type {number}
 */
const port = parseInt(process.env.PORT || '3000', 10);

/**
 * Menjalankan aplikasi Express
 * 
 * @function
 * @name listen
 * @inner
 * @param {number} port - Nomor port
 * @param {callback} callback - Fungsi callback setelah aplikasi Express dijalankan
 */
app.listen(port, callback);

/**
 * Fungsi callback setelah aplikasi Express dijalankan
 * 
 * @function
 * @name callback
 */
function callback() {
    console.log('####################################################################');
    console.log('NODE_ENV   : ' + process.env.NODE_ENV);
    console.log('address    : ' + JSON.stringify(this.address()));
    console.log('####################################################################');
}